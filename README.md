# devops_test_project

This project is a chance to demonstrate your technical skills!  Please submit a cloudformation template for the infrastructure (or equivalent if using other tools), along with configuration files and scripts used and the documentation as discussed in the Goal section below.

All resources used in this project are intended be available in the AWS free tier.

This repo hosts a cloudformation template to build out the infrastructure, and a very simple web and app tier to run on it. The repo contains three branches, one branch for each layer/host.

* master - cloudformation template

	You will need to create an SSH key to allow you access to the created instances.  The template uses the base CentOS 7 AMI from the us-west-2 region, you can change regions and AMIs as you see fit.  The template should output a URL that can be used to access the application and verify the stack is working.

* web - contains two static images to be served on the web layer

	You should install and configure nginx, Apache or another web server of your choice.  The key part of the configuration is to serve the /images directory in this branch and to forward on all other requests to the app layer.

* app - contains a simple perl application to test database connectivity

	*Note: we are not expecting you to debug or improve the application code.*  After checking out the branch, you can install the perl-App-cpanminus package dependencies using `cpanm HTTP::XSCookies@0.000007` and then `cpanm --installdeps .` in the application directory. The specific module install is needed due to a build/test issue with the perl Dancer2 module.
	You can then run up the application using `plackup -p <port> bin/app.psgi`. You will need to set the `DBHOST`, `USERNAME` and `PASSWORD` environment variables for the app to connect to the database.


# Goal

The main aim here is to turn this into a working 3 tier application stack, automating as much as possible and document the steps you took.

Feel free to use your tool(s) of choice; ec2-userdata, puppet, chef, ansible, etc.  The cloudformation template is only a starting point, feel free to revise, replace, use a different tool, etc - But please provide your finished configuration files.

It is also hoped that you will improve the security and scalability of the test project environment.

An ideal solution will be an updated repository that can be used to build a working stack with no manual intervention.

Please include a summary of your work, including an explanation of the changes you made (to the Cloudformation template, software configuration, security, etc) so we can follow your reasoning. Please also include a discussion on changes you would like to make to further improve the security and scalability of all parts of the system.

We will hold a final interview after receiving your project where you can give us a quick demonstration and we can ask some technical questions about your system.
